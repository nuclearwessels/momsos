#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <algorithm>

#include <QFile>
#include <QLabel>
#include <QTime>
#include <QPlainTextEdit>
#include <QJsonDocument>
#include <QJsonObject>
#include <QMessageBox>


const QString MainWindow::IPIFY_URL = "https://api.ipify.org?format=json";


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent, Qt::FramelessWindowHint),
    startUpMessageSent(false),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->helpButton->setStyleSheet("background: #FF0000");

    nam = new QNetworkAccessManager(this);

    connect(nam, &QNetworkAccessManager::finished, this, &MainWindow::networkFinished);

    connect(ui->helpButton, &QPushButton::clicked, this, &MainWindow::helpButton_clicked);
    connect(ui->testButton, &QPushButton::clicked, this, &MainWindow::testButton_clicked);
    connect(ui->claireButton, &QPushButton::clicked, this, &MainWindow::claireButton_clicked);

    timer = new QTimer(this);
    timer->setTimerType(Qt::CoarseTimer);
    timer->setInterval(500);
    connect(timer, &QTimer::timeout, this, &MainWindow::timerTick);
    timer->start();

    nextIpAddressCheck = QDateTime::currentDateTime();

    nextDailyMessage = QDateTime(QDate::currentDate(), QTime(12, 0, 0)).addDays(1);

    resize(800, 480);

    QFile configFile(QApplication::applicationDirPath() + "/config.json");
    bool ok = configFile.open(QFile::ReadOnly);
    if (ok)
    {
        QByteArray configData = configFile.readAll();
        configFile.close();

        QJsonDocument json = QJsonDocument::fromJson(configData);
        QJsonObject obj = json.object();
        apiUsername = obj.value("apiUsername").toString();
        apiPassword = obj.value("apiPassword").toString();
        jasonPhoneNumber = obj.value("jasonPhoneNumber").toString();
        marciePhoneNumber = obj.value("marciePhoneNumber").toString();
    }
    else
    {
        QMessageBox::critical(this, "No config file", "Unable to read configuration.");
        addLogMessage("Unable to read configuration", true);
        ui->toolBox->setCurrentIndex(1);
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::helpButton_clicked(bool)
{
    const QString HELP_MSG = "Help button pushed!";
    addLogMessage(HELP_MSG, false);
    sendSMS(HELP_MSG, jasonPhoneNumber);
#ifdef QT_NO_DEBUG
    sendSMS(HELP_MSG, marciePhoneNumber);
#endif
}

void MainWindow::testButton_clicked(bool)
{
    const QString TEST_MSG = "Test button pushed";

    addLogMessage(TEST_MSG, false);
    sendSMS(TEST_MSG, jasonPhoneNumber);
}


void MainWindow::claireButton_clicked(bool)
{
    const QString CLAIRE_MSG = "Claire is thinking about you.";

    addLogMessage(CLAIRE_MSG, false);
    sendSMS(CLAIRE_MSG, jasonPhoneNumber);
}


void MainWindow::timerTick()
{
    ui->timeLabel->setText(QTime::currentTime().toString("h:mm:ss AP"));
    ui->dateLabel->setText(QDate::currentDate().toString("MMM d, yyyy"));

    QDateTime now = QDateTime::currentDateTime();

    if (now >= nextIpAddressCheck)
    {
        QUrl url(IPIFY_URL);
        QNetworkRequest request(url);
        nam->get(request);

        nextIpAddressCheck = now.addDays(1);
    }

    if (now >= nextDailyMessage)
    {
        sendSMS("Daily test message", jasonPhoneNumber);
        addLogMessage("Daily test message sent", false);
        nextDailyMessage = nextDailyMessage.addDays(1);
    }

    if (!startUpMessageSent)
    {
        startUpMessageSent = true;
        sendSMS("MomSos starting up", jasonPhoneNumber);
    }
}



void MainWindow::sendSMS(const QString& msg, const QString& phoneNumber)
{
    QString time = QTime::currentTime().toString("h:mm:ss AP");
    QString wholeMsg = QString("%1: %2").arg(time, msg);
    QUrl url(QString("https://voip.ms/api/v1/rest.php?api_username=%1&api_password=%2&method=sendSMS&did=8585246364&dst=%3&message=%4")
                 .arg(apiUsername, apiPassword, phoneNumber, wholeMsg) );

    QNetworkRequest request(url);
    nam->get(request);
}



void MainWindow::networkFinished(QNetworkReply* reply)
{
    if (reply->error() == QNetworkReply::NoError)
    {
        if (reply->url().toDisplayString() == IPIFY_URL)
        {
            QByteArray response = reply->read(1024);
            QJsonDocument responseJson = QJsonDocument::fromJson(response);
            publicIpAddress = QHostAddress(responseJson["ip"].toString());
            addLogMessage(QString("IP Address: %1").arg(publicIpAddress.toString()), false);
        }
        else
        {
            QString response = reply->readAll();
            qDebug() << QString("Received response: %1").arg(response);
        }
    }
    else
    {   
        addLogMessage("Unable to send message to " + reply->url().toDisplayString(), true);
    }

    reply->deleteLater();

}


void MainWindow::addLogMessage(const QString& msg, bool error)
{
    QString now = QDateTime::currentDateTime().toString("h:mm:ss AP");
    QString wholeMsg = QString("%1: %2 %3").arg(now, error ? "Error:" : "", msg);

    ui->messages->appendPlainText(wholeMsg);
}


