#include "mainwindow.h"

#include <QApplication>
#include <QFile>
#include <QString>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QFile themeFile(QApplication::applicationDirPath() + "/Darkeum.qss");
    bool ok = themeFile.open(QIODevice::ReadOnly);
    if (ok)
    {
        QByteArray themeByteArray = themeFile.readAll();
        themeFile.close();

        QString themeStr(themeByteArray);
        a.setStyleSheet(themeStr);
    }

    MainWindow w;
    w.show();
    return a.exec();
}
