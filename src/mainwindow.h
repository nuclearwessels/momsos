#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QHostAddress>
#include <QMainWindow>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QTimer>


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

protected slots:
    void helpButton_clicked(bool);
    void testButton_clicked(bool);
    void claireButton_clicked(bool);
    void timerTick();

    void networkFinished(QNetworkReply*);

private:
    void sendSMS(const QString& msg, const QString& phoneNumber);
    void sendPublicIpAddressMessage();
    void addLogMessage(const QString& msg, bool error);

    bool startUpMessageSent;

    Ui::MainWindow *ui;
    QTimer* timer;
    QNetworkAccessManager* nam;

    QHostAddress publicIpAddress;

    QDateTime nextDailyMessage;
    QDateTime nextIpAddressCheck;

    QString apiUsername, apiPassword;
    QString jasonPhoneNumber, marciePhoneNumber;

//    static const QString HELP_MSG_TEXT, TEST_MSG_TEXT;
    static const QString IPIFY_URL;

};
#endif // MAINWINDOW_H
